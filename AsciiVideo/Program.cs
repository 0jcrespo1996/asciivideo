﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using ManagedBass;
using Slugify;

namespace AsciiVideo
{
    class Program
    {

        private static readonly char[] Characters = {'@', '#', 'S', '%', '?', '*', '+', ';', ':', ',', ' '};

        private static double _frameRate;
        private const int FrameSize = 150;
        private static bool _loop;

        private static int _volume = 20;

        private static MediaPlayer? _mediaPlayer;
        
        static void Main(string[] args)
        {
            try
            {
                _mediaPlayer = new MediaPlayer();
            }
            catch (Exception)
            {
                Console.WriteLine("Cannot create audio device.");
            }
            var file = args[0];
            _loop = args.Contains("--loop");

            _frameRate = GetVideoFrameRate(file);

            if (_frameRate.Equals(0))
            {
                Console.Write("Cannot determine framerate, please insert: ");
                var input = Console.ReadLine();
                if (string.IsNullOrWhiteSpace(input))
                    Environment.Exit(0);
                _frameRate = double.Parse(input, CultureInfo.InvariantCulture);
            }
            
            Console.WriteLine(_frameRate);
            var slug = new SlugHelper().GenerateSlug(Path.GetFileNameWithoutExtension(file));
            var root = Path.Join(slug);
            var txts = Path.Join(root, "txts");
            var frames = Path.Join(root, "frames");
            Directory.CreateDirectory(txts);
            Directory.CreateDirectory(frames);

            Console.WriteLine("Extracting frames...");
            ExtractFrames(file, frames);
            Console.WriteLine("Frames extracted");
            
            Console.WriteLine("Converting to ASCII");
            FramesToText(frames, txts);
            Console.WriteLine("Conversion to ASCII Completed");
            
            Console.WriteLine("Extracting audio");
            var audioPath = ExtractAudio(file, root);
            Console.WriteLine("Audio extracted");
            Play(txts, audioPath);
        }

        private static void PlayAudio(string audioFile)
        {
            _mediaPlayer?.LoadAsync(audioFile).GetAwaiter().GetResult();
            if (_mediaPlayer != null)
                _mediaPlayer.Volume = _volume / 100.0;
            _mediaPlayer?.Play();
        }

        private static void Play(string txtFiles, string audioFile)
        {
            var files = Directory.GetFiles(txtFiles, "*.txt");
            var stdOut = Console.OpenStandardOutput();
            
            var outputs = files.Select(File.OpenRead).ToArray();

            do
            {
                Console.BackgroundColor = ConsoleColor.Black;
                Console.ForegroundColor = ConsoleColor.White;
                Console.Clear();
                PlayAudio(audioFile);

                var span = TimeSpan.FromSeconds(1.0 / _frameRate);
                var sw = new Stopwatch();
                var i = 1;
                foreach (var file in outputs)
                {
                    sw.Restart();
                    Console.SetCursorPosition(0, 0);
                    file.CopyTo(stdOut);
                    sw.Stop();
                    var toSleep = (span - sw.Elapsed);
                    Console.WriteLine($"{1.0 / toSleep.TotalSeconds}\n{i++}/{outputs.Length}");
                    if (toSleep > TimeSpan.Zero)
                    {
                        Thread.Sleep(span - sw.Elapsed);
                    }
                }
            } while (_loop);
        }


        private static void FramesToText(string framesDir, string outputDir)
        {
            var frameFiles = Directory.GetFiles(framesDir, "*.jpg");
            if (Directory.GetFiles(outputDir, "*.txt").Length > 0) return;

            var created = 0;
            var r = Parallel.ForEach(frameFiles, frameFile =>
            {
                var frame = new Bitmap(frameFile);
                var width = frame.Width;
                var height = frame.Height;
                var aspectRatio = (height / (width * 2.5f));
                var newHeight = (int)(aspectRatio * FrameSize);
                var resized = new Bitmap(frame, FrameSize, newHeight);
                
                var path = Path.Join(outputDir, Path.ChangeExtension(Path.GetFileName(frameFile), ".txt"));
                var sb = new StringBuilder(resized.Width * resized.Height);
                for (var y = 0; y < resized.Height; y++)
                {
                    for (var x = 0; x < resized.Width; x++)
                    {
                        var pixel = resized.GetPixel(x, y);
                        var red = pixel.R;
                        var character = Characters[(int) Math.Floor(red / 25.0)];
                        sb.Append(character);
                    }

                    sb.AppendLine();
                }
                
                frame.Dispose();
                resized.Dispose();
                File.WriteAllText(path, sb.ToString());
                created++;
                Console.WriteLine($"{created}/{frameFiles.Length}");
            });
            
            SpinWait.SpinUntil(() => r.IsCompleted);
            GC.Collect();
        }
        
        private static void ExtractFrames(string file, string targetDir)
        {
            if (Directory.GetFiles(targetDir, "*.jpg").Length > 0) return;
            var process = Process.Start(new ProcessStartInfo
            {
                FileName = "ffmpeg",
                Arguments = $"-i \"{file}\" \"{Path.Join(targetDir, "frame%04d.jpg")}\" -hide_banner",
                CreateNoWindow = true,
                WindowStyle = ProcessWindowStyle.Hidden
            });

            process!.WaitForExit();
        }

        private static string ExtractAudio(string file, string targetDir)
        {
            var target = Path.Join(targetDir, Path.ChangeExtension(Path.GetFileName(file), ".mp3"));
            if (File.Exists(target)) return target;
            var process = Process.Start(new ProcessStartInfo
            {
                FileName = "ffmpeg",
                Arguments = $"-i \"{file}\" \"{target}\"",
                CreateNoWindow = true,
                WindowStyle = ProcessWindowStyle.Hidden
            });
            process!.WaitForExit();
            return target;
        }

        private static double GetVideoFrameRate(string path)
        {
            var process = Process.Start(new ProcessStartInfo
            {
                FileName = "ffprobe",
                Arguments =
                    $"-v 0 -of csv=p=0 -select_streams v:0 -show_entries stream=r_frame_rate \"{path}\"",
                RedirectStandardOutput = true,
                CreateNoWindow = true,
                UseShellExecute = false,
            });

            var line = process!.StandardOutput.ReadLine()?.Trim();
            if (string.IsNullOrWhiteSpace(line) || !Regex.IsMatch(line, @"[0-9]+\/[0-9]+"))
                return 0;
            var split = line.Split('/');
            return double.Parse(split[0], CultureInfo.InvariantCulture) / int.Parse(split[1]);
        }
    }
}